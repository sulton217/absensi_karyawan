<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use Session;

class CheckRoleController extends Controller
{
    public function login()
    {
        return view('auths.login');
    }


    public function postlogin(Request $request)
    {
        if(Auth::attempt($request->only('email','password'))){

            $data = User::where('email', $request->email)->first();
            if($data != null && $request->password != null){ 
            $decrypt = Hash::check($request->password,$data->password); 
            if($data->email == $request->email && $decrypt == $request->password){ 
                session()->put('email', $data->email); 
                session()->put('login',TRUE); 
                // Session::flash('sukses','Berhasil Login ');
                // return redirect('/dashboard');
                        if(Auth::user()->role=='admin'){
                            Session::flash('sukses','Berhasil Login ');
                            return redirect('/dashboard_admin');
                        }elseif(Auth::user()->role=='karyawan'){
                            
                            Session::flash('sukses','Berhasil Login ');
                            return redirect('/home');
                        }else{
                            Session::flash('salah','Peran Role Tidak Ditemukan !');
                            return redirect('/login');
                        } 
            }else{
                Session::flash('salah','Email atau Password Salah !');
                return redirect('/login'); 
            }
        }else{
            Session::flash('kosong','Mohon Lengkapi Email dan Password !');
            return redirect('/login');
        }

        }
        Session::flash('kosong','Mohon Lengkapi Email dan Password !');
        return redirect('/login');

    }


    public function logout()
    {
        session()->flush();
    Session::flash('logout','Akun Anda Berhasil Logout !');
    return redirect('/login');
    }

}
