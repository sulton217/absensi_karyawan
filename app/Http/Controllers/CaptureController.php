<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Capture_Image;
use DB;
use Auth;
use Carbon\carbon;
use File;
use Session;

class CaptureController extends Controller
{
    public function __construct()
    {

    }
    public function timeZone($location){
        return date_default_timezone_set($location);
    }
    
    public function capture_cam(Request $request){
        $this->timeZone('Asia/Jakarta');
        $date = date("Y-m-d");

        $user_id = Auth::user()->id;
        $img = $_POST['image'];
        $folderPath = "upload/";
      
        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
      
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';
      
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);
      
        DB::table('capture_image')->insert(
            [
                'id_absen' => $user_id,
                'nama_file' => $fileName,
                'date' => $date
            ]
        );
         return redirect('/home');
    } 
}
