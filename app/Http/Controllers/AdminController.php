<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Absen;

use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
	//
	public function timeZone($location){
		return date_default_timezone_set($location);
	}
	
                public function dashboard() //Halaman Depan / Landing Pages
            {


				$date = date("Y-m-d"); // 
				$AbsenHariIni=DB::table('absen')->where('date',$date)->count();
				$TotalFoto=DB::table('capture_image')->where('date',$date)->count();

				$sum = DB::table("users")->count();
				return view('admin.dashboard',
				compact('sum','AbsenHariIni','TotalFoto'));
				
				
            }

            public function crud_karyawan()
    {

	
    	// mengambil data dari table pegawai
    	$data = DB::table('users')->paginate(10);

    	// mengirim data pegawai ke view index
        return view('admin.data_karyawan', compact('data'));

    }

    // method untuk menampilkan view form tambah pegawai
public function tambah()
{

	// memanggil view tambah
	return view('admin.tambah');

}

// method untuk insert data ke table pegawai
public function store(Request $request)
{		
	// insert data ke table pegawai
	DB::table('users')->insert([
		'name' => $request->name,
		'email' => $request->email,
		'password' => bcrypt($request->password),
		'role' => $request->role
	]);
	// alihkan halaman ke halaman pegawai
	return redirect('/data_karyawan');

}

// method untuk hapus data pegawai
public function hapus($id)
{
	// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('users')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman pegawai
	return redirect('/data_karyawan');
}


public function data_edit()
{

		// mengambil data dari table pegawai
    	$data = DB::table('users')->paginate(10);

    	// mengirim data pegawai ke view index
        return view('admin.edit_karyawan', compact('data'));

}

// method untuk edit data pegawai
public function edit($id)
{
	// mengambil data pegawai berdasarkan id yang dipilih
	$data = DB::table('users')->where('id',$id)->get();
	// passing data pegawai yang didapat ke view edit.blade.php
    return view('admin.edit', compact('data'));

}

// update data pegawai
public function update(Request $request)
{
	// update data pegawai
	DB::table('users')->where('id',$request->id)->update([
		'name' => $request->name,
		'email' => $request->email,
		'password' => bcrypt($request->password),
		'role' => $request->role
	]);
	// alihkan halaman ke halaman pegawai
	return redirect('/data_karyawan');
}

// public function edit($id)
// {
// 	// mengambil data pegawai berdasarkan id yang dipilih
// 	$data = DB::table('users')->where('id',$id)->get();
// 	// passing data pegawai yang didapat ke view edit.blade.php
//     return view('admin.edit', compact('data'));

// }



public function detail($id)
{

	

	// mengambil data pegawai berdasarkan id yang dipilih
	$data = DB::table('absen')->where('user_id',$id)->paginate(10);
	$foto = DB::table('capture_image')->paginate(10);
	$rekap_alpha =  DB::table('absen')->where('user_id',$id)->sum('alpha');
	$rekap_ijin =  DB::table('absen')->where('user_id',$id)->sum('ijin');
	$rekap_sakit =  DB::table('absen')->where('user_id',$id)->sum('sakit');
	$total = $data->count();
	// $user = DB::table('users')->where('id', $id)->pluck('name');
	$user = DB::table('users')->where('id',$id)->get();
    return view('admin.detail_karyawan', compact('data','foto','rekap_alpha','rekap_ijin','rekap_sakit','total','user'));

}


public function ijin()
{
	$this->timeZone('Asia/Jakarta');
        $user_id = Auth::user()->id;
        $date = date("Y-m-d"); // 2017-02-01
        $time = date("H:i:s"); // 12:31:20

		DB::table('absen')->insert([
			'user_id' => $user_id,
			'date' => $date,
			'time_in' => $time,
            'ijin' => 1
		]);


		return redirect('/home');


}


public function sakit()
{
	$this->timeZone('Asia/Jakarta');
        $user_id = Auth::user()->id;
        $date = date("Y-m-d"); // 2017-02-01
        $time = date("H:i:s"); // 12:31:20

		DB::table('absen')->insert([
			'user_id' => $user_id,
			'date' => $date,
			'time_in' => $time,
            'sakit' => 1
		]);


		return redirect('/home');


}



}
