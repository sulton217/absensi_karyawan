<?php

namespace App\Http\Middleware;

use Closure;

use Session;

class CekSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->session()->exists('login')) {
            Session::flash('belum_login','Silahkan Login Terlebih Dahulu !');
        return redirect('/login');
        }

        return $next($request);
    }
}
