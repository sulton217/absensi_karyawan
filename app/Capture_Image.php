<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capture_Image extends Model
{
    //
    //
    protected $table = 'capture_image';
    protected $fillable = ['id', 'nama_file', 'keterangan'];
}
