@extends('layouts.app')

@section('content')

<div class="row page-header no-background no-shadow margin-b-0">
				<div class="col-lg-6 align-self-center ">
				  <h2>Dashboard Absensi</h2>
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">{{ $info['status']}}</li>
					</ol>
				</div>
				
		</div>
		
        <section class="main-content">

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-default">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"></div>

                        <div class="panel-body">
                            <table class="table table-responsive">
                                <form action="/absen" method="post">
                                    {{csrf_field()}}
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Kendala Hari ini" name="note">
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-flat btn-primary" name="btnIn" {{$info['btnIn']}}>ABSEN MASUK</button>
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-flat btn-primary" name="btnOut" {{$info['btnOut']}}>ABSEN KELUAR</button>
                                        </td>
                                        
                                        <small class="text-muted">*Kendala wajib di isi saat absen keluar</small>

                                    </tr>
                                </form>
                                <form action="/ijin" method="get">
                                {{csrf_field()}}
                                    <tr>
                                    <td>
                                            <input type="hidden" class="form-control" placeholder="Kendala Hari ini" name="note">
                                        </td>
                                    <td>
                                            <button type="submit" class="btn btn-flat btn-primary"  >IZIN UNTUK HARI INI</button>
                                        </td>
                                        </form>
                                        <form action="/sakit" method="get">
                                {{csrf_field()}}
                                        <td>
                                            <button type="submit" class="btn btn-flat btn-primary"  >IZIN SAKIT</button>
                                        </td>
</form>
</tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        <div class="card-body">
                            <div id="datatable2_wrapper" class="dataTables_wrapper no-footer">
                                <table id="datatable2" class="table table-striped dt-responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable2_info">
                                            <thead>
                                                <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 105px;">Tanggal</th>  
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 105px;">Jam Masuk</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 105px;">Jam Keluar</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 270px;">Note</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 270px;">Foto</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 25px;">A</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 25px;">I</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 25px;">S</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                    
                                            @forelse($data_absen as $absen)
                                <tr role="row" class="even">
                                    <td>{{$absen->date}}</td>
                                    <td>{{$absen->time_in}}</td>
                                    <td>{{$absen->time_out}}</td>
                                    <td>{{$absen->note}}</td>
                                    <td>
                                            @foreach($data_gambar as $foto)
                                                @if($foto->date == $absen->date)
                                                     <img src="{{asset('upload/'.$foto->nama_file)}}" width="100px" height="100px" >
                                                @endif
                                            @endforeach
                                    </td>

                                    <td>
                                        @if($absen->alpha == 1)
                                        <i class="fa fa-check"></i>
                                        @else <h3><strong>-</strong></h3>
                                        @endif
                                    </td>
                                    <td>
                                        @if($absen->ijin == 1)
                                        <i class="fa fa-check"></i>
                                        @else <h3><strong>-</strong></h3>
                                        @endif
                                    </td>
                                    <td>
                                        @if($absen->sakit == 1)
                                        <i class="fa fa-check"></i>
                                        @else <h3><strong>-</strong></h3>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4"><b><i>TIDAK ADA DATA UNTUK DITAMPILKAN</i></b></td>
                                </tr>
                            @endforelse
                    </tbody>
                                        </table>
                                        <div class="dataTables_info" id="datatable2_info" role="status" aria-live="polite">
                                        Halaman : {{ $data_absen->currentPage() }} dari {{ $data_absen->lastPage() }} halaman dengan jumlah {{ $data_absen->total() }} data  <br/>
                                        </div>
                                        <div class="dataTables_paginate paging_simple_numbers" id="datatable2_paginate">
                                        </span><a href="{{ $data_absen->previousPageUrl() }}" class="paginate_button next" aria-controls="datatable2" data-dt-idx="7" tabindex="0" id="datatable2_next">
                                            Previous
                                        </a><span>
                                            <a class="paginate_button current" aria-controls="datatable2" data-dt-idx="1" tabindex="0">{{ $data_absen->currentPage() }}</a>
                                      
                                        </span><a href="{{ $data_absen->nextPageUrl() }}" class="paginate_button next" aria-controls="datatable2" data-dt-idx="7" tabindex="0" id="datatable2_next">
                                            Next</a></div>
                                        
                                        </div>

        </div>
    </div>
</div>


</section>
@stop

