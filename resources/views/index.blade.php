@extends('layouts.app')

@section('content')


		
        <section class="main-content">

<div class="container">
    <h1 class="text-center">Silahkan Absen Menggunakan WebCam dan Berpakaian Rapi</h1>
   
    <form method="POST" action="/capture">
    {{csrf_field()}}

        <div class="row">
            <div class="col-md-6">
                <div id="my_camera"></div>
                <br/>
                <input type=button class="btn btn-border btn-primary btn-rounded box-shadow" value="Take Snapshot" onClick="take_snapshot()">
                <input type="hidden" name="image" class="image-tag">
            </div>
            <div class="col-md-6">
                <div id="results">Hasil Capture WebCam akan tampil di sini..</div>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-success btn-border btn-rounded box-shadow btn-large" >Selesai</button>
            </div>
        </div>
</br>
    </form>
</div>
  <!-- Configure a few settings and attach camera -->
<script language="JavaScript">
    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
  
    Webcam.attach( '#my_camera' );
  
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }
</script>

        </section>
@stop

