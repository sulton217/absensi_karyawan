@extends('layouts.app')

@section('content')



<div class="row page-header no-background no-shadow margin-b-0">
				<div class="col-lg-6 align-self-center ">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active"> 
                        @foreach($user as $u)  <h2>Rekap Absensi {{$u->name}}</h2> @endforeach
  
                        </li>
                        
					</ol>
				</div>
				
		</div>
		
        <section class="main-content">

		<h2>Total Rekap Absensi</h2>
<div class="row">

<div class="col">
                    <div class="widget bg-light padding-0">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center padding-15 bg-indigo">
                                <em class="icon-user-following fa-3x"></em>
                            </div>
                            <div class="col-xs-8 padding-15 text-right">
                                <h2 class="mv-0">{{$total}}</h2>
                                <div class="margin-b-0 text-muted">Total Absensi </div>
                            </div>
                        </div>
                    </div>
                </div>


        <div class="col">
                    <div class="widget bg-light padding-0">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center padding-15 bg-primary">
                                <em class="icon-ghost fa-3x"></em>
                            </div>
                            <div class="col-xs-8 padding-15 text-right">
                                <h2 class="mv-0">{{$rekap_alpha}}</h2>
                                <div class="margin-b-0 text-muted">Alpha</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="widget bg-light padding-0">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center padding-15 bg-teal">
                                <em class="icon-hourglass fa-3x"></em>
                            </div>
                            <div class="col-xs-8 padding-15 text-right">
                                <h2 class="mv-0">{{$rekap_ijin}}</h2>
                                <div class="margin-b-0 text-muted">Ijin</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                <div class="widget bg-light padding-0">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center padding-15 bg-success">
                                <em class="icon-plus fa-3x"></em>
                            </div>
                            <div class="col-xs-8 padding-15 text-right">
                                <h2 class="mv-0">{{$rekap_sakit}}</h2>
                                <div class="margin-b-0 text-muted">Sakit</div>
                            </div>
                        </div>
                    </div>
</div>


</div>
<h2>Riwayat Absensi</h2>
<div class="col-md-12">
    <div class="card">

	<div class="card-body">
                            <div id="datatable2_wrapper" class="dataTables_wrapper no-footer">
                                <table id="datatable2" class="table table-striped dt-responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable2_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 105px;">Tanggal</th>  
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 105px;">Jam Masuk</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 105px;">Jam Keluar</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 270px;">Note</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 270px;">Foto</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 25px;">A</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 25px;">I</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 25px;">S</th>

                                                </tr>
                                            </thead>

                                            <tbody>
                                    
                                            @forelse($data as $detail)
                                <tr role="row" class="even">
                                    <td>{{$detail->date}}</td>
                                    <td>{{$detail->time_in}}</td>
                                    <td>{{$detail->time_out}}</td>
                                    <td>{{$detail->note}}</td>
                                    

                                    <td>
                                            @foreach($foto as $f)
                                                @if($f->date == $detail->date)
                                                     <img src="{{asset('upload/'.$f->nama_file)}}" width="125px" height="125px" >
                                                @endif
                                            @endforeach
                                    </td>
                                    <td>
                                        @if($detail->alpha == 1)
                                        <i class="fa fa-check"></i>
                                        @else <h3><strong>-</strong></h3>
                                        @endif
                                    </td>
                                    <td>
                                        @if($detail->ijin == 1)
                                        <i class="fa fa-check"></i>
                                        @else <h3><strong>-</strong></h3>
                                        @endif
                                    </td>
                                    <td>
                                        @if($detail->sakit == 1)
                                        <i class="fa fa-check"></i>
                                        @else <h3><strong>-</strong></h3>
                                        @endif
                                    </td>
									
            @empty
                                <tr>
                                    <td colspan="4"><b><i>TIDAK ADA DATA UNTUK DITAMPILKAN</i></b></td>
                                </tr>

                              
                            
                            
                            @endforelse
                                            
                                    </tr> </tbody>
										</table>
										<!-- <a href="/karyawan/tambah"> + Tambah Pegawai Baru</a> -->

                                        <div class="dataTables_info" id="datatable2_info" role="status" aria-live="polite">
                                        Halaman : {{ $data->currentPage() }} dari {{ $data->lastPage() }} halaman dengan jumlah {{ $data->total() }} data  <br/>
                                        </div>
                                        <div class="dataTables_paginate paging_simple_numbers" id="datatable2_paginate">
                                        </span><a href="{{ $data->previousPageUrl() }}" class="paginate_button next" aria-controls="datatable2" data-dt-idx="7" tabindex="0" id="datatable2_next">
                                            Previous
                                        </a><span>
                                            <a class="paginate_button current" aria-controls="datatable2" data-dt-idx="1" tabindex="0">{{ $data->currentPage() }}</a>
                                      
                                        </span><a href="{{ $data->nextPageUrl() }}" class="paginate_button next" aria-controls="datatable2" data-dt-idx="7" tabindex="0" id="datatable2_next">
                                            Next</a>
                                            <div class="button"><a href="/data_karyawan" class="btn btn-default btn-rounded box-shadow">Kembali ke Halaman Data Karyawan</a>  </div>

                                        </div>

                                        </div>

		</div>

</div>

</div>

</secton>

@stop