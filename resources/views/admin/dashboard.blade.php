@extends('layouts.app')

@section('content')

<div class="row page-header no-background no-shadow margin-b-0">
				<div class="col-lg-6 align-self-center ">
				  <h2></h2>
					<ol class="breadcrumb">
						<li class="breadcrumb-item active"></li>
					</ol>
				</div>
				
		</div>
		
        <section class="main-content">

        </br>
@if ($message = Session::get('belum_logout'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
@endif
@if ($message = Session::get('sukses'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
@endif
<div class="row">
                <div class="col-md-12">
                    <div class="card">
					 <div class="card-header card-default">
							
							Dashboard
							<p class="text-muted">Menampilkan beberapa informasi mengenai Data Karyawan  dan Absensi yang telah dilakukan.  untuk <code>Hari ini </code>  </p>
                        </div>
						
              
      
                        <div class="card-body">
							<div class="row">
							<div class="col">
								<div class="widget bg-primary padding-15">
									<div class="row row-table">
										<div class="col-xs-8 padding-15 text-center">
											<h4 class="mv-0">{{$sum}}</h4>
											<div class="margin-b-0 ">Total Karyawan </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="widget bg-warning padding-15">
									<div class="row row-table">
										<div class="col-xs-8 padding-15 text-center">
											<h4 class="mv-0">{{$AbsenHariIni}}</h4>
											<div class="margin-b-0">Total  Absensi </div>
										</div>
									</div>
								</div>
							</div>
						
							<div class="col">
								<div class="widget bg-danger padding-15">
									<div class="row row-table">
										<div class="col-xs-8 padding-15 text-center">
											<h4 class="mv-0">{{$TotalFoto}}</h4>
											<div class="margin-b-0">Absensi Menggunakan Webcam</div>
										</div>
									</div>
								</div>
							</div>
						</div>
			

                     
                                <!-- <div id="datatable_wrapper" class="dataTables_wrapper no-footer"><div class="dataTables_scroll"><div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px; width: 100%;"><div class="dataTables_scrollHeadInner" style="box-sizing: content-box; width: 992px; padding-right: 17px;"><table class="table table-striped nowrap dataTable no-footer dtr-inline" width="100%" role="grid" style="margin-left: 0px; width: 992px;"><thead>
                                        <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 36px;" aria-sort="ascending" aria-label="
                                                ID
                                            : activate to sort column descending">
                                                <strong>ID</strong>
                                            </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 146px;" aria-label="
                                                Name
                                            : activate to sort column ascending">
                                                <strong>Name</strong>
                                            </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 201px;" aria-label="
                                                Email
                                            : activate to sort column ascending">
                                                <strong>Email</strong>
                                            </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 159px;" aria-label="
                                                Subject
                                            : activate to sort column ascending">
                                                <strong>Subject</strong>
                                            </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 62px;" aria-label="
                                                Status
                                            : activate to sort column ascending">
                                                <strong>Status</strong>
                                            </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 81px;" aria-label="
                                                Date
                                            : activate to sort column ascending">
                                                <strong>Date</strong>
                                            </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 82px;" aria-label="
                                                Action
                                            : activate to sort column ascending">
                                                <strong>Action</strong>
                                            </th></tr>
                                    </thead></table></div></div><div class="dataTables_scrollBody" style="position: relative; overflow: auto; width: 100%; height: 500px;"><table id="datatable" class="table table-striped nowrap dataTable no-footer dtr-inline" width="100%" role="grid" aria-describedby="datatable_info" style="width: 100%;"><thead>
                                        <tr role="row" style="height: 0px;"><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 36px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>ID</strong>
                                            </div></th><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 146px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>Name</strong>
                                            </div></th><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 201px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>Email</strong>
                                            </div></th><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 159px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>Subject</strong>
                                            </div></th><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 62px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>Status</strong>
                                            </div></th><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 81px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>Date</strong>
                                            </div></th><th class="sorting" aria-controls="datatable" rowspan="1" colspan="1" style="width: 82px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">
                                                <strong>Action</strong>
                                            </div></th></tr>
                                    </thead>
                                    
                                    <tbody>
                                        
                                       
										
										
										
										
										
										
										
										
                                    <tr role="row" class="odd">
                                            <td class="sorting_1">1425</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-2.png" width="30"> 
												John Doe
											</td>
                                            <td>john-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-warning">New</span>
                                            </td>
                                            <td>15/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="even">
                                            <td class="sorting_1">1426</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-1.png" width="30"> 
												Govinda Doe
											</td>
                                            <td>govinda-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-success">Complete</span>
                                            </td>
                                            <td>09/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="odd">
                                            <td class="sorting_1">1427</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-3.png" width="30"> 
												Megan Doe
											</td>
                                            <td>megan-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-warning">New</span>
                                            </td>
                                            <td>15/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="even">
                                            <td class="sorting_1">1428</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-4.png" width="30"> 
												Hritic Doe
											</td>
                                            <td>hritic-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-success">Complete</span>
                                            </td>
                                            <td>13/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="odd">
                                            <td class="sorting_1">1429</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-5.png" width="30"> 
												Bianca Doe
											</td>
                                            <td>bianca-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-success">Complete</span>
                                            </td>
                                            <td>11/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="even">
                                            <td class="sorting_1">1430</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-6.png" width="30"> 
												John Doe
											</td>
                                            <td>john-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-warning">New</span>
                                            </td>
                                            <td>14/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="odd">
                                            <td class="sorting_1">1431</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-7.png" width="30"> 
												Govinda Doe
											</td>
                                            <td>govinda-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-success">Complete</span>
                                            </td>
                                            <td>15/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="even">
                                            <td class="sorting_1">1432</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-8.png" width="30"> 
												Megan Doe
											</td>
                                            <td>megan-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-danger">Pending</span>
                                            </td>
                                            <td>12/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="odd">
                                            <td class="sorting_1">1433</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-3.png" width="30"> 
												Hritic Doe
											</td>
                                            <td>hritic-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-warning">New</span>
                                            </td>
                                            <td>16/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr><tr role="row" class="even">
                                            <td class="sorting_1">1434</td>
                                            <td>
												<img alt="user" class="media-box-object rounded-circle mr-2" src="assets/img/avtar-1.png" width="30"> 
												John Doe
											</td>
                                            <td>john-doe@domain.com</td>
											<td>How to change colors</td>
                                            <td class="text-center">
                                                <span class="label label-danger">Pending</span>
                                            </td>
                                            <td>15/07/2018</td>
                                            <td class="text-center">
												<button type="button" class="btn btn-sm btn-success"><i class="fa fa-reply"></i></button>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr></tbody>
                                </table></div></div><div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing 1 to 10 of 10 entries</div></div>
                        </div> -->
                    </div>
                </div>
            </div>
</section>
@stop