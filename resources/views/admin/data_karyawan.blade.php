@extends('layouts.app')

@section('content')
<div class="row page-header no-background no-shadow margin-b-0">
				<div class="col-lg-6 align-self-center ">
				  <h2>Data Absen Karyawan</h2>
					<ol class="breadcrumb">
						<li class="breadcrumb-item active"></li>
					</ol>
				</div>
				
		</div>
		
        <section class="main-content">

		</br>
@if ($message = Session::get('belum_logout'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
@endif
@if ($message = Session::get('sukses'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
@endif

<div class="col-md-12">
    <div class="card">

	<div class="card-body">
                            <div id="datatable2_wrapper" class="dataTables_wrapper no-footer">
                                <table id="datatable2" class="table table-striped dt-responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable2_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 171px;">Nama</th>  
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 270px;">Role</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 128px;">Email</th>
                                                    <!-- <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 43px;">Pasasword </th> -->
                                                    <th class="sorting" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 105px;">Opsi</th>
                                                    </tr>
                                            </thead>

                                            <tbody>

                                            @forelse($data as $karyawan)
                                <tr role="row" class="even">
                                    <td>{{$karyawan->name}}</td>
                                    <td>{{$karyawan->role}}</td>
                                    <td>{{$karyawan->email}}</td>

				<td>
                <a href="/karyawan/hapus/{{ $karyawan->id }}"><span class="label label-danger"><i class="fa fa-trash"> &nbsp; Hapus Karyawan</i></span></a>
                <a href="/karyawan/edit/{{ $karyawan->id }}"><span class="label label-success"><i class="fa fa-edit"> &nbsp; Edit Data Karyawan</i></span></a>
                <a href="/karyawan/detail/{{ $karyawan->id }}"><span class="label label-info"><i class="fa fa-search"> &nbsp; Rekap Absensi</i></span></a> 
                 </td>

            </td>
            @empty
                                @endforelse
                    </tbody>
										</table>
										<!-- <a href="/karyawan/tambah"> + Tambah Pegawai Baru</a> -->

                                        <div class="dataTables_info" id="datatable2_info" role="status" aria-live="polite">
                                        Halaman : {{ $data->currentPage() }} dari {{ $data->lastPage() }} halaman dengan jumlah {{ $data->total() }} data  <br/>
                                        </div>
                                        <div class="dataTables_paginate paging_simple_numbers" id="datatable2_paginate">
                                        </span><a href="{{ $data->previousPageUrl() }}" class="paginate_button next" aria-controls="datatable2" data-dt-idx="7" tabindex="0" id="datatable2_next">
                                            Previous
                                        </a><span>
                                            <a class="paginate_button current" aria-controls="datatable2" data-dt-idx="1" tabindex="0">{{ $data->currentPage() }}</a>
                                      
                                        </span><a href="{{ $data->nextPageUrl() }}" class="paginate_button next" aria-controls="datatable2" data-dt-idx="7" tabindex="0" id="datatable2_next">
                                            Next</a></div>
                                        
                                        </div>

		</div>
		
</div>
</div>

</secton>


@stop