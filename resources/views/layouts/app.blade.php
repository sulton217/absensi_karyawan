<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <title>Absensi Karyawan PKL</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Absensi Karyawan') }}</title> 

		<link rel="icon" type="image/png" sizes="16x16" href="{{url('image/Logo_Home.png')}}">
		
        <!-- Common Plugins -->
        <link href="{{url('assets/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
		
		<!-- Vector Map Css-->
        <link href="{{url('assets/lib/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
		
		<!-- Chart C3 -->
		<link href="{{url('assets/lib/chart-c3/c3.min.css')}}" rel="stylesheet">
		<link href="{{url('assets/lib/chartjs/chartjs-sass-default.css')}}" rel="stylesheet">

        <!-- DataTables -->
        <link href="{{url('assets/lib/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{url('assets/lib/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{url('assets/lib/toast/jquery.toast.min.css')}}" rel="stylesheet">
		
        <!-- Custom Css-->
		<link href="{{url('assets/scss/style.css')}}" rel="stylesheet">
		<link href="{{url('assets/scss/paginate.css')}}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    

    <!-- Styles -->
    <!-- <link href="/css/app.css" rel="stylesheet"> -->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    
</head>
<body>

@yield('content')
		
@include('layouts.navbar')
			
@include('layouts.right_sidebar')
		
@include('layouts.sidebar')
            	
		<!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
  

		

    <footer class="footer">
                <span>Copyright &copy; 2018 FixedPlus</span>
            </footer>

        
            

        <!-- ============================================================== -->
		<!-- 						Content End		 						-->
		<!-- ============================================================== -->
        
		<!-- Scripts -->
        <script src="/js/app.js"></script>
        <!-- Common Plugins -->
        <script src="{{url('assets/lib/jquery/dist/jquery.min.js')}}"></script>
		<script src="{{url('assets/lib/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{url('assets/lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{url('assets/lib/pace/pace.min.js')}}"></script>
        <script src="{{url('assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js')}}"></script>
        <script src="{{url('assets/lib/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{url('assets/lib/nano-scroll/jquery.nanoscroller.min.js')}}"></script>
        <script src="{{url('assets/lib/metisMenu/metisMenu.min.js')}}"></script>
        <script src="{{url('assets/js/custom.js')}}"></script>
			
        <!--Chart Script-->
        <script src="{{url('assets/lib/chartjs/chart.min.js')}}"></script>
		<script src="{{url('assets/lib/chartjs/chartjs-sass.js')}}"></script>

		<!--Vetor Map Script-->
		<script src="{{url('assets/lib/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
        <script src="{{url('assets/lib/vectormap/jquery-jvectormap-us-aea-en.js')}}"></script>
		
		<!-- Chart C3 -->
        <script src="{{url('assets/lib/chart-c3/d3.min.js')}}"></script>
        <script src="{{url('assets/lib/chart-c3/c3.min.js')}}"></script>
	
        <!-- Datatables-->
        <script src="{{url('assets/lib/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{url('assets/lib/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{url('assets/lib/toast/jquery.toast.min.js')}}"></script>
        <script src="{{url('assets/js/dashboard.js')}}"></script>
		
</body>
</html>
