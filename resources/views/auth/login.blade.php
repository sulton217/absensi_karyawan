

<!DOCTYPE html>
<html lang="en">
<head>
         <link href="{{url('/css/login.css')}}" rel="stylesheet">
         <link rel="icon" type="image/png" sizes="16x16" href="{{url('image/Logo_Home.png')}}">

         <style type='text/css'>
            h1, h2, h3, h4, h5, h6 {
                font-family: sans-serif;
                color: #f6f6f6;
                border-bottom: 1px solid rgb(200, 200, 200);
            }
        </style>
</head>

<body>


<!-- <div class="p-a white lt box-shadow"> -->

<!-- </div> -->

<div class="app">

    <div class="bg"></div>

    <form action="{{url('postlogin')}}" method="post">
    
    {{ csrf_field() }}

      <header>
        <img src="image/logo_login.png">
      </header>

      <div class="inputs">
        <input type="text" name="email" placeholder="username or email">
        <input type="password" name="password" placeholder="password">

        <p class="light"><a href="#">
            <!-- Forgot password? -->
        </a></p>
      </div>
@if ($message = Session::get('belum_login'))
    <h5>{{ $message }}</h5>
@endif  
@if ($message = Session::get('kosong'))
    <h5> {{ $message }}</h5>
@endif
@if ($message = Session::get('salah'))
    <h5>{{ $message }}</h5>
@endif
@if ($message = Session::get('register'))  
   <h5>{{ $message }}</h5>
@endif
@if ($message = Session::get('logout'))
    <h5>{{ $message }}</h5>
@endif
</br></br></br></br>
      <button type="Submit">Continue</button>
      
    </form>

    <footer>
      <!-- <p>Don't have an account? <a href="/register">Call Admin</a></p> -->
      <p>Don't have an account? <a href=#>Call Admin</a></p>

    </footer>


  </div>


  </body>
</html>
