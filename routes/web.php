<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', function () {
    return redirect('/login');
});


Route::get('/index', function () {
    return view('index');
});


Route::get('/login','CheckRoleController@login');
Route::post('postlogin', 'CheckRoleController@postlogin');


Auth::routes();

Route::get('/checkrole', 'CheckRoleController@checkrole');


Route::group(['middleware' => 'CekSession'], function () {
    ////////////////////////////////////////////////////////////////////////////////////

    Route::post('/capture', 'CaptureController@capture_cam');

 
Route::get('/dashboard_admin', 'AdminController@dashboard');
Route::get('/data_karyawan', 'AdminController@crud_karyawan');

Route::get('/karyawan/tambah','AdminController@tambah');
Route::post('/karyawan/store','AdminController@store');
Route::get('/karyawan/data_edit','AdminController@data_edit');
Route::get('/karyawan/edit/{id}','AdminController@edit');
Route::post('/karyawan/update','AdminController@update');
Route::get('/karyawan/hapus/{id}','AdminController@hapus');
Route::get('/karyawan/detail/{id}','AdminController@detail');


Route::get('/home', 'HomeController@index');
Route::post('/absen', 'HomeController@absen');
Route::get('/ijin', 'AdminController@ijin');
Route::get('/sakit', 'AdminController@sakit');



Route::get('/logout','CheckRoleController@logout');

});

